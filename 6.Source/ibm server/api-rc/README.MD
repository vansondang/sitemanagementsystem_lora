# Prerequisite

1. You have to install [NodeJs] and [NPM](https://nodejs.org/en/download/)

## Run the API in local host

1. Clone this repository
2. `cd` into the app directory
3. Run `npm install` to install the dependencies
4. Update the default variables in file `.env` if you change the database.
5. Run `npm start` to start the API.

## Push to IBM Bluemix Cloud Foundry.

1. Update your [manifest.yml](manifest.yml)
2. Follow [this guide](https://console.ng.bluemix.net/docs/starters/install_cli.html)
