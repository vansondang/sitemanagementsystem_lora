#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

//-------- Customise these values -----------

const char* ssid = "NSETEC 1";
const char* password = "88888888";

#define ORG "s2vwfm"
#define DEVICE_TYPE "nodemcu"
#define DEVICE_ID "600194178888"
#define TOKEN "5WfTaisSM6zOWpgTJa"

char server[] = ORG ".messaging.internetofthings.ibmcloud.com";
char authMethod[] = "use-token-auth";
char token[] = TOKEN;
char clientId[] = "d:" ORG ":" DEVICE_TYPE ":" DEVICE_ID;

char topic[] = "iot-2/evt/status/fmt/json";
char subtopic[] = "iot-2/cmd/test/fmt/json";
// char subtopic[] = "iot-2/evt/test/fmt/json";
//char topic[] = "iot-2/cmd/test/fmt/json";

// char subtopic[] = "iot-2/cmd/fly/fmt/json";

//iot-2/evt/<event-id>/fmt/<format> Publish event - event-id choose own value
//iot-2/cmd/<cmd-type>/fmt/<format-id> Subscribe commands choose own value
//iot-2/type/<type-id>/id/<device-id>/evt/<event-id>/fmt/<format-id> Subscribe events
//iot-2/type/arduino/

//iot-2/type/device_type/id/device_id/evt/event_id/fmt/format_string - publish device event
//iot-2/type/device_type/id/device_id/cmd/command_id/fmt/format_string - publish device cmd
//iot-2/type/device_type/id/device_id/evt/event_id/fmt/format_string - subscribe device event
//iot-2/type/device_type/id/device_id/cmd/command_id/fmt/format_string - subscribe device cmd
//iot-2/type/device_type/id/device_id/mon - subscribe device stt
//iot-2/app/app_id/mon - subscribe application stt

void wifiConnect();
void mqttConnect();
void jsonGenerate();
//void initManagedDevice();
void handlePayload(byte *payload, unsigned int payloadLength);
void publishData();
void callback(char* topic, byte* payload, unsigned int payloadLength);

int stt = 1;
int ctrl = 0;
char rid[] = "B201";

uint8_t MAC_array[6];
char MAC_char[18];

const int BUFFER_SIZE = JSON_OBJECT_SIZE(5) + JSON_ARRAY_SIZE(0);
//Object size JsonBuffer on ESP8266
//JsonArray of N element: 8 + 12*N
//JsonObject of N element: 8+ 16*N
char buffer[BUFFER_SIZE];

WiFiClient wifiClient;
PubSubClient client(server, 1883, callback, wifiClient);

void wifiConnect() 
{
  Serial.print("Connecting to "); Serial.print(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
}

//void mqttConnect() {
//  if (!client.connected()) {
//    Serial.print("Reconnecting MQTT client to "); Serial.println(server);
//    while (!client.connect(clientId, authMethod, token)) {
//      Serial.print(".");
//      delay(500);
//    }
//    initManagedDevice();
//    Serial.println();
//  }
//}

 void mqttConnect() 
 {
   while (!client.connected()) {
     Serial.println("Attempting MQTT connection..."); // Attempt to connect
     if (client.connect(clientId, authMethod, token)) {
       //clientId: clientId
       //username: authMethod
       //password: token
       Serial.println("Connected to IBM Bluemix!");
     }
     else {
       uint16_t rc_time = 5000; //5000ms = 5s
       Serial.print("Failed to connect to IBM Bluemix! - rc = ");
       Serial.println(client.state());

       switch (client.state()) {
         case -4:
           Serial.println("MQTT_CONNECTION_TIMEOUT - the server didn't respond within the keepalive time.");
           break;
         case -3:
           Serial.println("MQTT_CONNECTION_LOST - the network connection was broken.");
           break;
         case -2:
           Serial.println("MQTT_CONNECT_FAILED - the network connection failed.");
           break;
         case -1:
           Serial.println("MQTT_DISCONNECTED - the client is disconnected cleanly.");
           break;
         case 0:
           Serial.println("MQTT_CONNECTED - the cient is connected.");
           break;
         case 1:
           Serial.println("MQTT_CONNECT_BAD_PROTOCOL - the server doesn't support the requested version of MQTT.");
           break;
         case 2:
           Serial.println("MQTT_CONNECT_BAD_CLIENT_ID - the server rejected the client identifier");
           break;
         case 3:
           Serial.println("MQTT_CONNECT_UNAVAILABLE - the server was unable to accept the connection.");
           break;
         case 4:
           Serial.println("MQTT_CONNECT_BAD_CREDENTIALS - the username/password were rejected.");
           break;
         case 5:
           Serial.println("MQTT_CONNECT_UNAUTHORIZED - the client was not authorized to connect.");
           break;
         default:
           break;
       }

       Serial.print("Reconnecting in "); Serial.print(rc_time / 1000); Serial.println("s");
       delay(rc_time);
     }
    initManagedDevice(); 
   }
 }

void initManagedDevice() {
 if (client.subscribe(subtopic)) {
   Serial.println("subscribe to all event OK");
 } else {
   Serial.println("FAIL");
 }
} 


void jsonGenerate() 
{
  StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonObject& d = root.createNestedObject("d");
  d["id"] = MAC_char;
  d["rid"] = rid;
  d["stt"] = stt;
  d["ctrl"] = ctrl;
  root.printTo(buffer, sizeof(buffer));
}

void publishData() {

  jsonGenerate();

  Serial.print("Sending payload: "); Serial.println(buffer); 

  if (client.publish(topic, buffer)) {
  Serial.println("Publish OK");
  } else {
    Serial.println("Publish FAILED");
  }
}

void handlePayload(byte *payload, unsigned int payloadLength)
{
  String json = "";

  for(int count=0; count<payloadLength; count++){
      json = json + (char)payload[count];
  }
  Serial.println(json);
  Serial.println(payloadLength);

  StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject((char*) json.c_str());
  
  if(!root.success()){
      Serial.println("parseObject() failed!");
      return;
  }
  
  JsonObject& d = root["d"];
  const char* id = d["id"];
  const char* rid = d["rid"];
  int stt = d["stt"];
  int ctrl = d["ctrl"];

  
  Serial.println(stt);
  Serial.println(ctrl);
  Serial.println(id);
  Serial.println(rid);
}

void callback(char* topic, byte* payload, unsigned int payloadLength) 
{
  Serial.print("callback invoked for topic: "); Serial.println(subtopic);

  for (int i = 0; i < payloadLength; i++) {
    Serial.print((char)payload[i]); 
  }
  Serial.print("\n");
  handlePayload(payload, payloadLength);
}


void setup()
{
  Serial.begin(115200);
  Serial.println();
  wifiConnect();

  WiFi.macAddress(MAC_array);
  for (int i = 0; i < sizeof(MAC_array); ++i) {
    sprintf(MAC_char, "%s%02x", MAC_char, MAC_array[i]);
  }
  Serial.print("ID: "); Serial.println(MAC_char);

  mqttConnect();
}

void loop()
{
  if (!client.loop()) {
    mqttConnect();
  }
   publishData();

  delay(5000);
}
