'use strict';

const express = require('express');
const router = express.Router();
const Cloudant = require('cloudant');
const debug = require('debug')('hackanoi:server');

const account = process.env.CLOUDANT_USERNAME;
const password = process.env.CLOUDANT_PASSWORD;

const cloudant = Cloudant({account, password, plugin:'promises'}, (err, db) => {
  if (! err) {
    debug('Connect to Cloudant successfully');
  }
});
const db = cloudant.db.use('devices');

router.get('/', (req, res, next) => {
  const defaultOptions = {
    selector: {
      timestamp: {
        '$gt': 0
      }
    },
    limit: 10,
    sort: [
      {
        timestamp: 'desc'
      }
    ]
  };

  db.find(defaultOptions)
    .then(data => {
      res.json({
        status: 'success',
        data: data.docs
      });
    })
    .catch(err => {
      debug(err);
      res.json(err)
    })
});

router.get('/:did', (req, res, next) => {
  const options = {
    "selector": {
      "did": {
        "$eq": `${req.params.did}`
      }
    },
    "sort": [
      {
        "timestamp": "desc"
      }
    ],
    "limit": 10
  }

  db.find(options)
    .then(data => {
      res.json({
        status: 'success',
        data: data.docs
      });
    })
    .catch(err => {
      debug(err);
      res.json(err)
    })
})

module.exports = router;